package cc.game.SteampunkZelda;

import cc.game.SteampunkZelda.entities.Entity;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: calv
 * Date: 03/05/13
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
public class State {

    private Object[] data;

    public State(Entity entity) {
        this.data = entity.getData();
    }

    public void save() {
        try {
            System.out.println("Saving data to file.");
            DataOutputStream dos = new DataOutputStream(new FileOutputStream("./entitydata.dat"));
            dos.write(toByteArray(this.data));
            dos.flush();
            dos.close();
        } catch (FileNotFoundException e) {
            System.err.println("Couldn't find file.");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Couldn't save entity data.");
            e.printStackTrace();
        }
    }

    private byte[] toByteArray(Object[] data) {
        byte[] bytes = new byte[1];
        bytes[0] = (byte) 122324;
        return bytes;
    }
}
