package cc.game.SteampunkZelda;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.RoundedRectangle;

/**
 * User: Calv
 * Date: 24/07/13
 * Time: 13:52
 */
public class ButtonData {
    private final RoundedRectangle button;
    private Color colour;
    private String message;

    public ButtonData(RoundedRectangle rectangle, String message, Color colour) {
        this.button = rectangle;
        this.message = message;
        this.colour = colour;
    }

    public RoundedRectangle getButton() {
        return this.button;
    }

    public Color getColour() {
        return this.colour;
    }

    public void setColour(Color colour) {
        this.colour = colour;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
