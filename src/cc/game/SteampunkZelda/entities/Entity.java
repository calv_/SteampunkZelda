package cc.game.SteampunkZelda.entities;

import cc.game.SteampunkZelda.State;
import cc.game.SteampunkZelda.SteampunkZelda;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;

import java.util.ArrayList;

/**
 * User: Calv
 * Date: 21/02/13
 * Time: 21:34
 */
public abstract class Entity {
    protected SpriteSheet sheet;
    protected Vector3f velocity = new Vector3f(0f,0f,0f);
    protected ArrayList<Animation> animations = new ArrayList<Animation>();
    protected Rectangle boundingBox;
    protected String name;

    protected float posX;
    protected float posY;
    protected float posZ;
    protected float health;
    protected int SPRITE_WIDTH;
    protected int SPRITE_HEIGHT;
    protected int DEFAULT_DURATION = 50;

    protected int[] FRAME_COUNT = {};
    protected int[] FRAME_RATE = {};
    protected SteampunkZelda gameInstance;
    private float scale;

    protected float maxSpeed;

    public Entity(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT, int[] FRAME_COUNT, int[] FRAME_RATE, float health, float scale) {
        this.gameInstance = gameInstance;
        this.posX = posX;
        this.posY = posY;
        this.SPRITE_WIDTH = SPRITE_WIDTH;
        this.SPRITE_HEIGHT = SPRITE_HEIGHT;
        this.FRAME_COUNT = FRAME_COUNT;
        this.FRAME_RATE = FRAME_RATE;
        this.health = health;
        this.scale = scale;
        this.boundingBox = new Rectangle(this.posX, this.posY, SPRITE_WIDTH * this.scale, SPRITE_HEIGHT * this.scale);
    }

    public Entity(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT, int[] FRAME_COUNT, int[] FRAME_RATE, float health) {
        this.gameInstance = gameInstance;
        this.posX = posX;
        this.posY = posY;
        this.SPRITE_WIDTH = SPRITE_WIDTH;
        this.SPRITE_HEIGHT = SPRITE_HEIGHT;
        this.FRAME_COUNT = FRAME_COUNT;
        this.FRAME_RATE = FRAME_RATE;
        this.health = health;
        this.scale = 1;
        this.boundingBox = new Rectangle(this.posX, this.posY, SPRITE_WIDTH * this.scale, SPRITE_HEIGHT * this.scale);
    }

    protected void setUpSpriteSheet(String imageLocation) {
        try {
            this.sheet = new SpriteSheet(new Image(imageLocation), this.SPRITE_WIDTH, this.SPRITE_HEIGHT);
            System.out.println("Loaded, sheet: " + this.sheet.getResourceReference() + ", size: " + this.sheet.getWidth() + "x" + this.sheet.getHeight() +
                               ", with an animation count of: " + FRAME_COUNT.length);
            for (int animationCounter=0; animationCounter<this.FRAME_COUNT.length; animationCounter++) {
                Animation animation = new Animation();
                for (int i=0; i<this.FRAME_COUNT[animationCounter]; i++) {
                    animation.addFrame(this.sheet.getSprite(i, animationCounter).getScaledCopy(scale), this.FRAME_RATE[animationCounter]);
                }
                animation.setLooping(true);
                this.animations.add(animation);
            }
        } catch (SlickException e){
            System.out.println("Couldn't load " + imageLocation + " into an animation.");
            e.printStackTrace();
        }
    }

    public void update(GameContainer gc, int deltaTime) {
        updateBoundingBox();
    }

    private void updateBoundingBox() {
        this.boundingBox.setX(this.posX);
        this.boundingBox.setY(this.posY);
    }

    /**
     * Basic Rendering code, it will render the first animation in the animations arraylist, override to add custom animating logic.
     */
    public void render(){
        Animation animation;
        animation = this.animations.get(0);
        animation.setAutoUpdate(!SteampunkZelda.isPaused());
        animation.draw(this.posX, this.posY);
    }

    public void render(int ANIMATION_TYPE){
        Animation animation;
        animation = this.animations.get(ANIMATION_TYPE);
        animation.setAutoUpdate(!SteampunkZelda.isPaused());
        animation.draw(this.posX, this.posY);
    }

    /**
     * Checks to see if the current entity is colliding with another entity specified.
     * @param other The entity that is to be check if colliding with the current entity.
     * @return Returns a boolean stating true if a collision, or false if no collision.
     */
    public boolean collides(Entity other){
        if (this.boundingBox.intersects(other.boundingBox)) {
            return true;
        }
        return false;
    }

    /**
     * Debug method to draw the bounding box for the entity.
     * @param graphics the graphics instance to draw the rectangle with.
     */
    public void renderRect(Graphics graphics) {
        graphics.drawRect(this.boundingBox.getX(), this.boundingBox.getY(), this.boundingBox.getWidth(), this.boundingBox.getHeight());
    }

    public Rectangle getBoundingBox() {
        return this.boundingBox;
    }

    public String getName() {
        return this.name;
    }

    /**
     * Method used for saving that retrieves all data about the entity.
     */
    public void saveState() {
        State state = new State(this);
        state.save();
    }

    public Object[] getData() {
        Object[] objects = new Object[13];
        objects[0] = this.sheet;
        objects[1] = this.velocity;
        objects[2] = this.animations;
        objects[3] = this.boundingBox;
        objects[4] = this.name;
        objects[5] = this.posX;
        objects[6] = this.posY;
        objects[7] = this.posZ;
        objects[8] = this.SPRITE_WIDTH;
        objects[9] = this.SPRITE_HEIGHT;
        objects[10] = this.FRAME_COUNT;
        objects[11] = this.FRAME_RATE;
        objects[12] = this.scale;

        return objects;
    }

    public float getHealth() {
        return health;
    }

    public float getSpeed() {
        return Math.abs(this.velocity.length());
    }

    /**
     * Returns the position of the character
     * @return float positionX, float positionY, float positionZ
     */
    public float[] getPosition() {
        return new float[]{this.posX, this.posY, this.posZ};
    }
}
