package cc.game.SteampunkZelda.entities;

import cc.game.SteampunkZelda.SteampunkZelda;

/**
 * User: Calv
 * Date: 15/06/13
 * Time: 19:13
 */
public class Giduuf extends HostileEntity {

    public Giduuf(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, "giduuf", 1f);
        this.maxSpeed = 2f;
    }

}
