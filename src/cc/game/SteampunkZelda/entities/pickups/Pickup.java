package cc.game.SteampunkZelda.entities.pickups;

import cc.game.SteampunkZelda.SteampunkZelda;
import cc.game.SteampunkZelda.entities.Entity;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.GameContainer;

/**
 * Created with IntelliJ IDEA.
 * User: Calv
 * Date: 23/02/13
 * Time: 16:41
 * To change this template use File | Settings | File Templates.
 */
public class Pickup extends Entity {

    private static final int[] FRAME_COUNT = new int[] {
            4,  // Bobbing
    };

    private static final int[] FRAME_RATE = new int[] {
            83,  // Bobbing
    };
    private float magnetDistance = 80f;

    public Pickup(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, FRAME_COUNT, FRAME_RATE, 20f);
        this.setUpSpriteSheet("res/entities/pickups/unimplemented_pickup.png");
        this.maxSpeed = 1f;
    }

    public Pickup(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT, float scale) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, FRAME_COUNT, FRAME_RATE, 20f, scale);
        this.setUpSpriteSheet("res/entities/pickups/unimplemented_pickup.png");
        this.maxSpeed = 1f;
    }

    @Override
    public void update(GameContainer gc, int deltaTime) {
        super.update(gc, deltaTime);
        updateAI();
    }

    protected void updateAI() {
        updateAIMovement();
    }

    private void updateAIMovement() {
        float[] playerPosition = this.gameInstance.getPlayer().getPosition();
        Vector3f distanceVector = new Vector3f(playerPosition[0] - this.getPosition()[0], playerPosition[1] - this.getPosition()[1], playerPosition[2] - this.getPosition()[2]);

        if (distanceVector.length() <= magnetDistance) {
            distanceVector.scale(Math.min(1, this.maxSpeed / distanceVector.length()));
            this.posX += Math.min(distanceVector.x, this.maxSpeed);
            this.posY += Math.min(distanceVector.y, this.maxSpeed);
        }
    }
}
