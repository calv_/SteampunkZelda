package cc.game.SteampunkZelda.entities;

import cc.game.SteampunkZelda.SteampunkZelda;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.GameContainer;

/**
 * Created with IntelliJ IDEA.
 * User: Calv
 * Date: 23/02/13
 * Time: 09:22
 * To change this template use File | Settings | File Templates.
 */
public abstract class HostileEntity extends Entity {

    private static final int[] FRAME_COUNT = new int[] {
            4,  // Running
            4,  // Idle
            4,  // Jump
    };

    private static final int[] FRAME_RATE = new int[] {
            83,  // Running
            83,  // Idle
            83,  // Jump
    };

    private float cooldownTimer = 0;
    private float cooldownFactor = 50f;
    private float activeRadius;

    public HostileEntity(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT, String name, float scale) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, FRAME_COUNT, FRAME_RATE, scale);
        this.name = name;
        this.activeRadius = 120;
        this.setUpSpriteSheet("res/entities/hostile/" + this.name + ".png");
    }

    public HostileEntity(SteampunkZelda gameInstance, int posX, int posY, int SPRITE_WIDTH, int SPRITE_HEIGHT, String name, float scale, float activeRadius) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, FRAME_COUNT, FRAME_RATE, scale);
        this.name = name;
        this.activeRadius = activeRadius;
        this.setUpSpriteSheet("res/entities/hostile/" + this.name + ".png");
    }

    @Override
    public void update(GameContainer gc, int deltaTime) {
        this.updateCooldown(deltaTime);
        super.update(gc, deltaTime);
        this.updateAI();
    }

    protected void updateAI() {
        this.updateAIMovement();
    }

    private void updateAIMovement() {
        float[] playerPosition = this.gameInstance.getPlayer().getPosition();
        Vector3f distanceVector = new Vector3f(playerPosition[0] - this.posX, playerPosition[1] - this.posY, playerPosition[2] - this.posZ);

        if (distanceVector.length() < this.activeRadius) {
            distanceVector.scale(Math.min(1, this.maxSpeed / distanceVector.length()));
            this.posX += Math.min(distanceVector.x, this.maxSpeed);
            this.posY += Math.min(distanceVector.y, this.maxSpeed);
        }
    }

    private void updateCooldown(int deltaTime) {
        if (this.collides(this.gameInstance.getPlayer()) && this.cooldownTimer == 0) {
            float playerSpeed = this.gameInstance.getPlayer().getSpeed();
            this.cooldownTimer += (Math.max(playerSpeed * cooldownFactor / 2 * deltaTime, cooldownFactor * deltaTime));
        }
        if (this.cooldownTimer != 0) {
            if (this.cooldownTimer > 0) {
                this.cooldownTimer -= deltaTime;
            }
            else {
                this.cooldownTimer = 0;
            }
        }
    }

    public float getCooldownTimer() {
        return this.cooldownTimer;
    }
}
