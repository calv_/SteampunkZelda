package cc.game.SteampunkZelda.entities;

import cc.game.SteampunkZelda.SteampunkZelda;
import cc.game.SteampunkZelda.entities.pickups.Pickup;
import org.newdawn.slick.*;

import java.util.ArrayList;

/**
 * User: Calv
 * Date: 22/02/13
 * Time: 09:37
 */
public class Player extends Entity {
    private final float friction = 0.45f;
    private final float movementSpeed = 1.0f;

    private static final int ANIMATION_RUNNING = 0;
    private static final int ANIMATION_IDLE = 1;
    private static final int ANIMATION_JUMPING = 2;

    private static final int SPRITE_WIDTH = 16;
    private static final int SPRITE_HEIGHT = 16;

    private static int[] FRAME_COUNT = new int[]{
            4,  //Running
            4,  //Idle
            4   //Jumping
    };
    private static int[] FRAME_RATE = new int[]{
            83, //Running
            83, //Idle
            83  //Jumping
    };
    private boolean isJumping;
    private boolean isIdle;
    private boolean isDead = false;

    public Player(SteampunkZelda gameInstance, int posX, int posY) {
        super(gameInstance, posX, posY, SPRITE_WIDTH, SPRITE_HEIGHT, FRAME_COUNT, FRAME_RATE, 20f, 1f);
        this.setUpSpriteSheet("res/entities/players/player.png");
        this.name = "player";
        this.maxSpeed = 4f;
    }

    @Override
    public void update(GameContainer gc, int deltaTime) {
        super.update(gc,deltaTime);
        Input input = gc.getInput();

        updateMovement(input);
        updateCollisions();
    }

    @Override
    public void render() {
        Animation animation;
        if (isJumping) animation = this.animations.get(ANIMATION_JUMPING);
        else if (isIdle) animation = this.animations.get(ANIMATION_IDLE);
        else animation = this.animations.get(ANIMATION_RUNNING);
        if (Math.min(velocity.x, 0) == 0) ; //running right
        else if (Math.max(velocity.x, 0) == 0) ; //running left
        animation.setAutoUpdate(!SteampunkZelda.isPaused());
        animation.draw(this.posX, this.posY);
    }

    private void updateMovement(Input input) {
        if ((input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W) || input.isControllerUp(Input.ANY_CONTROLLER)))
            onUp();
        if ((input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S) || input.isControllerDown(Input.ANY_CONTROLLER)))
            onDown();
        if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A) || input.isControllerLeft(Input.ANY_CONTROLLER))
            onLeft();
        if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D) || input.isControllerRight(Input.ANY_CONTROLLER))
            onRight();

//        Sets the gravity, if the player is in the air, their velocity is affected by gravity,
//        if the player is on the ground the velocity is zero.
        if (this.posY > (this.gameInstance.getMapSize().getY()) - this.boundingBox.getHeight()) {
            this.velocity.y = 0;
            this.posY = this.gameInstance.getMapSize().getY() - this.boundingBox.getHeight();
        }
        else if (this.posY < 0) {
            this.velocity.y = 0;
            this.posY = 0;
        }

        if (this.velocity.x > 0)
            this.velocity.x -= friction;
        if (this.velocity.x < 0)
            this.velocity.x += friction;
        if ((Math.abs(this.velocity.x) < Math.abs(friction)) && this.velocity.x != 0)
            this.velocity.x = 0;

        if (this.velocity.y > 0)
            this.velocity.y -= friction;
        if (this.velocity.y < 0)
            this.velocity.y += friction;
        if ((Math.abs(this.velocity.y) < Math.abs(friction)) && this.velocity.y != 0)
            this.velocity.y = 0;

        if (this.posX > this.gameInstance.getMapSize().getX() - this.boundingBox.getWidth()) {
            this.posX = (this.gameInstance.getMapSize().getX() - this.boundingBox.getWidth());
            this.velocity.x = 0;
        }
        else if (this.posX < 0) {
            this.posX = 0;
            this.velocity.x = 0;
        }
        if (this.posY > this.gameInstance.getMapSize().getY() + this.boundingBox.getHeight())
            this.posY = (this.gameInstance.getMapSize().getY() - this.boundingBox.getHeight());

        this.posX += this.velocity.x;
        this.posY -= this.velocity.y;

        this.isIdle = false;
        this.isJumping = false;

        if ((this.getSpeed() <= 0) && !isJumping) {
            this.isIdle = true;
        }
    }

    private void updateCollisions() {
        ArrayList<Entity> entities = SteampunkZelda.getEntities();
        for (int entityIterator = 0; entityIterator < entities.size(); entityIterator++) {
            Entity entity = entities.get(entityIterator);
            if (entity instanceof HostileEntity) {
                HostileEntity hostileEntity = (HostileEntity) entity;
                if (this.collides(hostileEntity) && ((HostileEntity) entity).getCooldownTimer() == 0) {
                    System.out.println(entity.getName() + " attacked " + this.getName());
                    updateHealth(-1);
                }
            }
            else if (entity instanceof Pickup) {
                Pickup pickup = (Pickup) entity;
                if (this.collides(pickup)) {
                    entities.remove(entityIterator);
                }
            }
        }
    }

    private void updateHealth(int deltaHealth) {
        if ((this.health + deltaHealth) <= 0) {
            this.health = 0;
            this.isDead = true;
        }
        else {
            this.health += deltaHealth;
        }

        if (this.gameInstance.getDebug()) System.out.println("Player health: " + this.health);
    }

    private void onUp() {
        if ((this.velocity.y + this.movementSpeed) > this.maxSpeed) {
            this.velocity.y = this.maxSpeed;
        } else {
            this.velocity.y += this.movementSpeed;
        }
    }

    private void onDown() {
        if (Math.abs(this.velocity.y - this.movementSpeed) > this.maxSpeed) {
            this.velocity.y = -this.maxSpeed;
        } else {
            this.velocity.y -= this.movementSpeed;
        }
    }

    private void onLeft() {
        if (Math.abs(this.velocity.x - this.movementSpeed) > this.maxSpeed) {
            this.velocity.x = -this.maxSpeed;
        } else {
            this.velocity.x -= movementSpeed;
        }
    }

    private void onRight() {
        if ((this.velocity.x + this.movementSpeed) > this.maxSpeed) {
            this.velocity.x = this.maxSpeed;
        } else {
            this.velocity.x += movementSpeed;
        }
    }

    public boolean isDead() {
        return isDead;
    }

}
