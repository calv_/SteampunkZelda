package cc.game.SteampunkZelda.screen;

import cc.game.SteampunkZelda.SteampunkZelda;
import org.newdawn.slick.*;

/**
 * Created with IntelliJ IDEA.
 * User: calv
 * Date: 05/04/13
 * Time: 18:19
 * To change this template use File | Settings | File Templates.
 */
public class SplashScreen extends Screen {

    private final Image bgImage;
    private long existed = 0;

    public SplashScreen(SteampunkZelda game) throws SlickException {
        super(game, 1280, 720);
        this.bgImage = new Image("res/screens/splash.png");
    }

    @Override
    public void update(GameContainer gameContainer, int deltaTime) throws SlickException {
        this.existed += deltaTime;
        if (this.existed > 5000) this.game.setScreen(new SaveSelectScreen(this.game));
    }

    @Override
    public void render(GameContainer gameContainer) {
        this.bgImage.draw(0,0,this.game.getWindowSize().getX(),this.game.getWindowSize().getY());
    }
}
