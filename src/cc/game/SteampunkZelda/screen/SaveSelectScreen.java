package cc.game.SteampunkZelda.screen;

import cc.game.SteampunkZelda.SteampunkZelda;
import cc.game.SteampunkZelda.entities.Player;
import cc.game.SteampunkZelda.screen.gamescreen.GameScreenIntroLevel;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.RoundedRectangle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Calv
 * Date: 24/02/13
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
public class SaveSelectScreen extends Screen {

    private final Image bgImage;
    private String text = "";
    private int textWidth;
    private int textHeight;
    private ArrayList<Rectangle> rectangles = new ArrayList<Rectangle>();
    private HashMap<Rectangle, Boolean> rectangleMap = new HashMap<Rectangle, Boolean>();
    private ArrayList<Player> playerList = new ArrayList<Player>();

    public SaveSelectScreen(SteampunkZelda game) throws SlickException{
        super(game, 1280, 720);
        this.bgImage = new Image("res/screens/playerselect.png");
    }

    private void setupButtons() {
        Player player;
        Rectangle bb;

//        Formal main character
        player = new Player(this.game, 150, 150);
        bb = player.getBoundingBox();
        playerList.add(player);
        rectangles.add(bb);
        rectangleMap.put(bb, false);

//        Formal main character
        player = new Player(this.game, 150, 200);
        bb = player.getBoundingBox();
        playerList.add(player);
        rectangles.add(bb);
        rectangleMap.put(bb, false);
    }

    public void onStart() throws SlickException {
        GameContainer gameContainer = this.game.getGameContainer();
        Graphics graphics = gameContainer.getGraphics();

        this.text = "Player Selection!";

        Font font = graphics.getFont();
        this.textWidth = font.getWidth(this.text);
        this.textHeight = font.getHeight(this.text);

        this.setupButtons();
    }

    public void onStop() throws SlickException {
        if (!playerList.isEmpty()) {
            for (Player player : playerList) {
                rectangleMap.remove(player.getBoundingBox());
            }
        }
    }

    public void update(GameContainer gameContainer, int deltaTime) throws SlickException {
        if (gameContainer.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
            int x = Math.round(gameContainer.getInput().getMouseX() / game.getScaleX());
            int y = Math.round(gameContainer.getInput().getMouseY() / game.getScaleY());
            System.out.println("Mouse clicked @:  x:" + x + ", y:" + y);
            for (Rectangle rect : rectangles) {
                if (x >= rect.getMinX() && x <= rect.getMaxX()) {
                    if (y >= rect.getMinY() && y <= rect.getMaxY()) {
                        System.out.println("Rectangle clicked.");
                        this.rectangleMap.put(rect, true);
                    }
                }
            }
        }

        Player player = null;

        for (Rectangle rect : rectangles) {
            if (this.rectangleMap.get(rect) == true) {
                for (Player p : playerList) {
                    if (p.getBoundingBox() == rect) {
                        player = p;
                    }
                }
            }
        }

        if (player != null) {
            this.game.setScreen(new GameScreenIntroLevel(this.game));
            this.game.setupInitialEntities(player);
        }
    }

    public void render(GameContainer gameContainer) {
        this.bgImage.draw();
        Graphics graphics = gameContainer.getGraphics();
        graphics.setColor(Color.gray);
        RoundedRectangle rect = new RoundedRectangle(50, 50, game.getWidth() - 100, game.getHeight() - 100, 20);
        graphics.draw(rect);
        graphics.fill(rect);
        graphics.setColor(Color.blue);
        graphics.drawString(this.text, (game.getWidth() / 2) - (this.textWidth / 2), (110) - (this.textHeight / 2));

        for (Player player : playerList) {
            player.render();
            graphics.draw(player.getBoundingBox());
        }

        drawSaveInfo(gameContainer, graphics);
    }

    private void drawSaveInfo(GameContainer gameContainer, Graphics graphics) {
        graphics.setColor(Color.white);
        Font font = graphics.getFont();

        ArrayList<String> messages = new ArrayList<String>();
        messages.add("Save 1");
        messages.add("Save 2");
        int posX = 200;
        int posY = 105;

        for (String message : messages)  {
            int messageWidth = font.getWidth(message);
            int messageHeight = font.getHeight(message);
            posY += 50;

            graphics.drawString(message, posX - (messageWidth / 2), posY - (messageHeight / 2));
        }
    }
}
