package cc.game.SteampunkZelda.screen.gamescreen;

import cc.game.SteampunkZelda.SteampunkZelda;

/**
 * User: calv
 * Date: 30/03/13
 * Time: 22:26
 */
public class GameScreenIntroLevel extends GameScreen {

    public GameScreenIntroLevel(SteampunkZelda game) {
        super(game, 1, 1280, 720);
    }

}
