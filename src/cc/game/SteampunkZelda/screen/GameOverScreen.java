package cc.game.SteampunkZelda.screen;

import cc.game.SteampunkZelda.ButtonData;
import cc.game.SteampunkZelda.SteampunkZelda;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.RoundedRectangle;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * User: calv
 * Date: 03/04/13
 * Time: 16:25
 */
public class GameOverScreen extends Screen {

    private final Image bgImage;
    private ArrayList<ButtonData> buttons = new ArrayList<ButtonData>();

    public GameOverScreen(SteampunkZelda game) throws SlickException {
        super(game, 1280, 720);
        this.bgImage = new Image("res/screens/gameover.png");
        this.buttons.add(new ButtonData(new RoundedRectangle((this.game.getWindowSize().getX() / 4) * 3 - 100f, (this.game.getWindowSize().getY() / 4) * 3, 100f, 25f, 2f), "Exit", Color.cyan));
    }

    public void render(GameContainer gameContainer) {
        Graphics graphics = gameContainer.getGraphics();
        Font font = graphics.getFont();
        this.bgImage.draw();

        graphics.setAntiAlias(true);

        for (ButtonData buttonData : buttons) {
            RoundedRectangle button = buttonData.getButton();
            String message = buttonData.getMessage();
            Color buttonColour = buttonData.getColour();

            graphics.setColor(buttonColour);

            graphics.fillRoundRect(button.getMinX(), button.getMinY(), button.getWidth(), button.getHeight(), (int) Math.floor(button.getCornerRadius()));

            if (buttonColour == Color.cyan) {
                graphics.setColor(Color.black);
            }
            else {
                graphics.setColor(Color.white);
            }
            graphics.drawString(message, button.getCenterX() - (font.getWidth(message) / 2), button.getCenterY() - (font.getHeight(message) / 2));
        }
    }

    public void update(GameContainer gameContainer, int deltaTime) throws SlickException {
        float mouseX = gameContainer.getInput().getMouseX();
        float mouseY = gameContainer.getInput().getMouseY();
        for (ButtonData buttonData : buttons) {
            RoundedRectangle button = buttonData.getButton();

            if (mouseX < button.getMaxX() * this.game.getScaleX() && mouseX > button.getMinX() * this.game.getScaleX()
                    && mouseY < button.getMaxY()  * this.game.getScaleY() && mouseY > button.getMinY()  * this.game.getScaleY()) {
                buttonData.setColour(Color.blue);
                if (gameContainer.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
                    if (buttonData.getMessage() == "Exit") {
                        System.exit(0);
                    }
                }
            }
            else {
                if (buttonData.getColour() != Color.cyan) {
                    buttonData.setColour(Color.cyan);
                }
            }
        }
    }
}
